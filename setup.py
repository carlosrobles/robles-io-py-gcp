import io
import os
import re

from setuptools import find_packages
from setuptools import setup


def read(filename):
    filename = os.path.join(os.path.dirname(__file__), filename)
    text_type = type(u"")
    with io.open(filename, mode="r", encoding='utf-8') as fd:
        return re.sub(text_type(r':[a-z]+:`~?(.*?)`'), text_type(r'``\1``'), fd.read())


setup(
    name="robles-io-py-gcp",
    version="0.2.1",
    url="https://gitlab.com/carlosrobles/robles-io-py-gcp",
    license='MIT',

    author="Carlos Robles",
    author_email="carlos@robles.io",

    description="Package for interacting with Google Cloud resources",
    long_description=read("README.md"),

    packages=find_packages(exclude=('tests',)),

    install_requires=[
        'google-api-python-client>=2.113.0; python_version>="3"',
        'google-cloud-datastore>=2.19.0; python_version>="3"',
        'tenacity>=8.2.3; python_version>="3"',
        'robles-io-logger @ https://pypi.robles.io/api/package/robles-io-logger/robles_io_logger-0.1.0-py3-none-any.whl#egg=robles-io-logger-0.1.0',
    ],

    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11',
        'Programming Language :: Python :: 3.12',
        'Programming Language :: Python :: 3.13',
    ],
)
