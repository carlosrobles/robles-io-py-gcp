"""robles-io-py-gcp - Package for interacting with Google Cloud resources"""

__version__ = '0.2.0'
__author__ = 'Carlos Robles <carlos@robles.io>'
__all__ = ['gce', 'datastore']
