import json

from google.cloud import datastore as gcp_datastore
from google.cloud.datastore import query
from tenacity import retry, stop_after_attempt, wait_exponential, wait_fixed, wait_random

class Datastore:
    """
    A class to represent a GCP Datastore connection object

    Attributes:
        client: google api datastore client
            Client for interacting with the GCP datastore API

    Methods:
        delete_entity(kind, name):
            Deletes a single datastore entity with the given kind and name
        get_entity_by_name(kind, name):
            Returns a single datastore entity with the given kind and name
        get_entities_by_filter(kind, filter):
            Returns a list of datastore entities that match the given kind and filter
        update_entity(kind, name, properties):
            Update an existing entity.
        write_entity(kind, name, properties):
            Write an entity. Writing an existing entity overwrites it.
    """
    def __init__(self, project, credentials, namespace=None):
        """
        Constructs Datastore object instance

        Args:
            credentials: google.auth.credentials Credentials
                GCP Credentials object instance
            project: str
                Target GCP project
        """
        self.client = gcp_datastore.Client(project=project, credentials=credentials, namespace=namespace)

    @retry(stop=stop_after_attempt(3),
           wait=wait_fixed(3) + wait_random(min=1, max=5))
    def delete_entity(self, kind, name):
        """
        Deletes a single datastore entity with the given kind and name

        Args:
            kind: str
                The kind of the entity
            name: str
                The name of the entity
        """
        key = self.client.key(kind, name)
        self.client.delete(key)

    @retry(stop=stop_after_attempt(3),
           wait=wait_fixed(3) + wait_random(min=1, max=5))
    def get_entity_by_name(self, kind, name):
        """
        Returns a single datastore entity with the given kind and name

        Args:
            kind: str
                The kind of the entity
            name: str
                The name of the entity

        Returns:
            Datastore entity object
        """
        kind = kind
        key = self.client.key(kind, name)
        return self.client.get(key)

    @retry(stop=stop_after_attempt(3),
           wait=wait_fixed(3) + wait_random(min=1, max=5))
    def get_entities_by_filter(self, kind, filter):
        """
        Returns a list of datastore entities that match the given kind and filter

        Args:
            kind: str
                The kind of the entity
            filter: dict(list(tuples))
                A dictionary containing the datastore filter information in the following format
                {
                    "or": [
                        ("region", "=", "us-central1"),
                        ("region", "=", "us-east1"),
                    ],
                    "and": [
                        ("zone", "=", "b"),
                        ("machine_types", "IN", ["N2"]),
                    ],
                }
                This example filter returns entities with region "us-central1" or "us-east1"
                not in zone "b" with "N2" in machine_types

        Returns:
            List of Datastore entity objects
        """
        kind = kind
        filter_query = self.client.query(kind=kind)
        and_queries = []
        or_queries = []
        for k, v in filter.items():
            for entry in v:
                property_name = entry[0]
                operator = entry[1]
                value = entry[2]
                if k == "or":
                    or_queries.append(query.PropertyFilter(property_name, operator, value))
                if k == "and":
                    and_queries.append(query.PropertyFilter(property_name, operator, value))
        if filter.get("and", ""):
            or_queries.append(query.And(and_queries))
        or_query = query.Or(or_queries)
        filter_query.add_filter(filter=or_query)
        return list(filter_query.fetch())

    def update_entity(self, kind, name, properties):
        """
        Update an existing entity.

        Args:
            kind: str
                The kind of the entity
            name: str
                The unique name of the entity
            properties: dict
                The properties of the entity to update
        """
        with self.client.transaction():
            key = self.client.key(kind, name)
            entity = self.client.get(key)
            for k,v in properties.items():
                entity[k] = v
        self.client.put(entity)

    @retry(stop=stop_after_attempt(3),
           wait=wait_fixed(3) + wait_random(min=1, max=5))
    def write_entity(self, kind, name, properties):
        """
        Write an entity. Writing an existing entity overwrites it.

        Args:
            kind: str
                The kind of the entity
            name: str
                The unique name of the entity
            properties: dict
                The properties of the entity
        """
        key = self.client.key(kind, name)
        entity = gcp_datastore.Entity(key=key)
        entity.update(properties)
        self.client.put(entity)
