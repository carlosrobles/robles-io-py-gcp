import json
import os
import robles_io_logger as log

from datetime import datetime
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from tenacity import retry, stop_after_attempt, wait_exponential, wait_fixed, wait_random

gcp_creds_file = '/tmp/google_creds'
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = gcp_creds_file

log = log.get_logger()


class ResourceInUseError(Exception):
    """
    Raised when a GCP resource is in use
    """
    pass


class GCE:
    """
    A class to represent a GCE entity

    Attributes:
        compute_client: google api compute resource client
            Client for interacting with the GCE API

    Methods:
        delete_disk(name, project, zone, name):
            Deletes the specified disk
        delete_image(name, project):
            Deletes the specified image
        delete_instance(name, project, zone):
            Deletes the specified instance
        delete_mig(name, project, zone):
            Deletes the specified mig
        delete_packer_disks(project, zone):
            Deletes any disks named "packer*" in the given zone for the given project
        delete_packer_instances(project, zone):
            Deletes any instances named "packer*" in the given zone for the given project
        list_disks(project, zone, filter=None):
            Returns all (filtered) disks in the given zone for the given project
        list_images(project, zone, filter=None):
            Returns all (filtered) images in the given project
        list_instances(project, zone):
            Returns all instances in the given zone for the given project
        list_migs(project, zone, filter=None):
            Returns all (filtered) migs in the given zone for the given project
        get_zone_from_mig(mig):
            Returns the zone for a given mig
        list_zones(project, region):
            Returns all zones in the given region
    """
    def __init__(self, credentials):
        """
        Constructs gce object instance

        Args:
            credentials: google.auth.credentials.Credentials
                GCP credentials object instance
        """
        self.compute_client = build('compute', 'v1', credentials=credentials)


    @retry(stop=stop_after_attempt(3),
           wait=wait_fixed(3) + wait_random(min=1, max=5))
    def create_disk(self, project, zone, disk_body=None):
        """
        Creates a disk in the given zone in the given project

        Args:
            project: str
                The project to search
            zone: str
                The zone to search

        Kwargs:
            disk_body: dict
                The optional disk creation parameters

        Returns:
            Operation resource for the disk creation
        """
        try:
            response = self.compute_client.disks().insert(project=project, zone=zone, body=disk_body).execute()
        except:
            log.exception('error creating disk')
            raise

        return response


    @retry(stop=stop_after_attempt(3),
           wait=wait_fixed(3) + wait_random(min=1, max=5))
    def create_image(self, project, image_body=None):
        """
        Creates an image in the given project

        Args:
            project: str
                The project to search
            zone: str
                The zone to search

        Kwargs:
            image_body: dict
                The optional image creation parameters

        Returns:
            Operation resource for the image creation
        """
        try:
            response = self.compute_client.images().insert(project=project, body=image_body).execute()
        except:
            log.exception('error creating image')
            raise

        return response


    @retry(stop=stop_after_attempt(3),
           wait=wait_fixed(3) + wait_random(min=1, max=5))
    def create_instance(self, project, zone, instance_body=None):
        """
        Creates an instance in the given zone in the given project

        Args:
            project: str
                The project to search
            zone: str
                The zone to search

        Kwargs:
            instance_body: dict
                The optional instance creation parameters
D
        Returns:
            Operation resource for the instance creation
        """
        try:
            response = self.compute_client.instances().insert(project=project, zone=zone, body=instance_body).execute()
        except:
            log.exception('error creating instance')
            raise

        return response


    @retry(stop=stop_after_attempt(3),
           wait=wait_exponential(multiplier=1, min=4, max=10))
    def delete_disk(self, name, project, zone):
        """
        Deletes the specified disk

        Args:
            name: str
                The name of the disk to delete
            project: str
                The target project
            zone: str
                The target zone

        Returns:
            Operation resource for the disk deletion
        """
        response = {}
        try:
            response = self.compute_client.disks().delete(
                project=project,
                zone=zone,
                disk=name
            ).execute()
        except HttpError as error:
            error_reason = json.loads(error.content)['error']['errors'][0]['reason']
            if error_reason == 'resourceInUseByAnotherResource':
                log.info("Disk is still in use. Trying again...")
                raise ResourceInUseError
        except:
            log.exception('Error deleting disk')
            raise

        return response
        

    @retry(stop=stop_after_attempt(3),
           wait=wait_exponential(multiplier=1, min=4, max=10))
    def delete_image(self, name, project):
        """
        Deletes the specified image

        Args:
            name: str
                The name of the image to delete
            project: str
                The target project

        Returns:
            Operation resource for the image deletion
        """
        response = {}
        try:
            response = self.compute_client.images().delete(
                project=project,
                image=name
            ).execute()
        except:
            log.exception('Error deleting image')
            raise

        return response


    @retry(stop=stop_after_attempt(3),
           wait=wait_fixed(3) + wait_random(min=1, max=5))
    def delete_instance(self, name, project, zone):
        """
        Deletes the specified instance

        Args:
            name: str
                The name of the instance to delete
            project: str
                The target project
            zone: str
                The target zone

        Returns:
            Operation resource for the instance deletion
        """
        response = {}
        try:
            response = self.compute_client.instances().delete(
                project=project,
                zone=zone,
                instance=name
            ).execute()
        except:
            log.exception(f"error deleting instance {instanceID}")
            raise

        return response


    @retry(stop=stop_after_attempt(3),
           wait=wait_fixed(3) + wait_random(min=1, max=5))
    def delete_mig(self, name, project, zone):
        """
        Deletes the specified mig

        Args:
            name: str
                The name of the mig to delete
            project: str
                The target project
            zone: str
                The target zone

        Returns:
            Operation resource for the mig deletion
        """
        try:
            response = self.compute_client.instanceGroupManagers().delete(project=project, zone=zone, instanceGroupManager=name).execute()
        except:
            log.exception(f"error deleting mig {name}")
        
        return response


    @retry(stop=stop_after_attempt(3),
           wait=wait_fixed(3) + wait_random(min=1, max=5))
    def delete_packer_disks(self, project, zone):
        """
        Deletes any disks named "packer*" in the given zone for the given project

        Args:
            project: str
                The project to search
            zone: str
                The zone to search
        """
        disks = self.list_disks(project, zone)
        for disk in disks:
            if disk['name'].startswith('packer'):
                log.info(f"Deleting disk {disk['name']}")
                self.delete_disk(project, zone, disk['name'])


    def delete_packer_instances(self, project, zone):
        """
        Deletes any instances named "packer*" in the given zone for the given project

        Args:
            project: str
                The project the instances are in
            zone: str
                The zone the instances are in
        """
        compute_items = self.list_instances(project, zone)
        for instance in compute_items:
            if instance['name'].startswith('packer'):
                log.info(f"Deleting instance {instance['name']}")
                self.delete_instance(project, zone, instance['name'])


    @retry(stop=stop_after_attempt(3),
           wait=wait_fixed(3) + wait_random(min=1, max=5))
    def list_disks(self, project, zone, filter=None):
        """
        Returns all (filtered) disks in the given zone for the given project

        Args:
            project: str
                The project to search
            zone: str
                The zone to search
        
        Kwargs:
            filter: str
                The optional filter to use for the search

        Returns:
            List of disk resource dictionaries
        """
        try:
            response = self.compute_client.disks().list(filter=filter, project=project, zone=zone).execute()
        except:
            log.exception('error listing disks')
            raise

        return response.get('items', [])


    @retry(stop=stop_after_attempt(3),
           wait=wait_fixed(3) + wait_random(min=1, max=5))
    def list_images(self, project, filter=None):
        """
        Returns all (filtered) images in the given project

        Args:
            project: str
                The project to search

        Kwargs:
            filter: str
                The optional filter to use for the search

        Returns:
            List of image resource dictionaries
        """
        try:
            response = self.compute_client.images().list(filter=filter, project=project).execute()
        except:
            log.exception('error listing images')
            raise

        return response.get('items', [])


    @retry(stop=stop_after_attempt(3),
           wait=wait_fixed(3) + wait_random(min=1, max=5))
    def list_instances(self, project, zone):
        """
        Returns all instances in the given zone for the given project

        Args:
            project: str
                The project to search
            zone: str
                The zone to search

        Returns:
            List of instance resource dictionaries
        """
        try:
            response = self.compute_client.instances().list(project=project, zone=zone).execute()
        except:
            log.exception('error listing instances')
            raise

        return response.get('items', [])


    @retry(stop=stop_after_attempt(3),
           wait=wait_fixed(3) + wait_random(min=1, max=5))
    def list_migs(self, project, zone, filter=None):
        """
        Returns all (filtered) migs in the given zone for the given project

        Args:
            project: str
                The project to search
            zone: str
                The zone to search
        
        Kwargs:
            filter: str
                Search filter to apply

        Returns:
            migs: list
                List of mig resource dictionaries
        """
        migs = []
        try:
            request = self.compute_client.instanceGroupManagers().list(filter=filter, project=project, zone=zone)
        except:
            log.exception('error listing migs')
            raise
        else:
            while request is not None:
                response = request.execute()
                if response.get('items', []):
                    for mig in response['items']:
                        migs.append(mig)
                    try:
                        request = self.compute_client.instanceGroupManagers().list_next(previous_request=request, previous_response=response)
                    except:
                        log.exception('error paginating migs')
                        raise
                else:
                    break

        return migs


    def get_zone_from_mig(self, mig):
        """
        Returns the zone for a given mig

        Args:
            mig: str
                The mig resource dictionary to parse

        Returns:
            zone: str
                The zone the mig is in
        """
        zone = mig.get('zone', '').split('/')[-1]
        return zone


    @retry(stop=stop_after_attempt(3),
           wait=wait_fixed(3) + wait_random(min=1, max=5))
    def list_zones(self, project, region):
        """
        Returns all zones in the given region

        Args:
            project: str
                The project to search
            region: str
                The region to search

        Returns:
            zones: list
                List of zone resource dictionaries in region
        """
        try:
            response = self.compute_client.zones().list(project=project, filter=f"(region = \"https://www.googleapis.com/compute/v1/projects/{project}/regions/{region}\")").execute()
        except:
            log.exception('error getting zones')
            raise
        
        return response.get('items', [])
