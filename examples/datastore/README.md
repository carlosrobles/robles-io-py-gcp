# Datastore Examples

To run these example scripts:

Install the necessary packages
```commandline
pip install robles-io-py-gcp --extra-index-url https://pypi.robles.io/simple
pip install robles-io-logger --extra-index-url https://pypi.robles.io/simple
```

Set the necessary environment variables

```commandline
export GOOGLE_APPLICATION_CREDENTIALS='<path to google cloud private key json file>'
export GOOGLE_PROJECT_ID='<target google cloud project id>'
```