import google.auth
import json
import os

from robles_io_py_gcp import datastore
from robles_io_logger import get_logger

project = os.environ['GOOGLE_PROJECT_ID']


def main():
    logger = get_logger()

    credentials, _ = google.auth.default()
    ds = datastore.Datastore(project, credentials)

    kind = "GCPRegions"
    ds_filter = {
        "or": [
            ("region", "=", "us-central1"),
            ("region", "=", "us-east1"),
        ],
        "and": [
            ("zone", "=", "b"),
            ("machine_types", "IN", ["N2"]),
        ],
    }

    entities = ds.get_entities_by_filter(kind, ds_filter)
    print(json.dumps(entities, indent=4))


if __name__ == '__main__':
    main()
