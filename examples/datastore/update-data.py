import datetime
import google.auth
import os
import uuid

from robles_io_py_gcp import datastore
from robles_io_logger import get_logger

project = os.environ['GOOGLE_PROJECT_ID']


def main():
    logger = get_logger()

    credentials, _ = google.auth.default()
    ds = datastore.atastore(project, credentials)

    kind = "Temperature"
    initial_recording = {
        "date": datetime.datetime(2023, 8, 27),
        "high": "94",
        "low": "75",
    }

    logger.info(f"writing entity to datastore")
    # generate random unique uuid id/name for every record
    entity_id = str(uuid.uuid4())
    ds.write_entity(kind, entity_id, initial_recording)

    updated_recording = {
        "high": "96",
    }

    logger.info(f"updating entity to datastore")
    ds.update_entity(kind, entity_id, updated_recording)


if __name__ == '__main__':
    main()
