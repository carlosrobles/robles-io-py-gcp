import google.auth
import os
import uuid

from robles_io_py_gcp import datastore
from robles_io_logger import get_logger

project = os.environ['GOOGLE_PROJECT_ID']


def main():
    logger = get_logger()

    credentials, _ = google.auth.default()
    ds = datastore.Datastore(project, credentials)

    kind = "GCPRegions"
    property_list = [
        {
            "machine_types": ["E2", "N2", "N2D", "C3", "T2D", "T2A", "N1", "M1", "M2", "M3", "H3", "C2", "C2D", "A2", "G2"],
            "location": "Council Bluffs, Iowa, North America",
            "region": "us-central1",
            "zone": "a",
        },
        {
            "machine_types": ["E2", "N2", "N2D", "C3", "T2D", "T2A", "N1", "M1", "M2", "M3", "C2", "C2D", "A2", "G2"],
            "location": "Council Bluffs, Iowa, North America",
            "region": "us-central1",
            "zone": "b",
        },
        {
            "machine_types": ["E2", "N2", "N2D", "C3", "T2D", "N1", "M1", "M2", "C2", "C2D", "A2"],
            "location": "Council Bluffs, Iowa, North America",
            "region": "us-central1",
            "zone": "c",
        },
        {
            "machine_types": ["E2", "N2", "N2D", "T2D", "T2A", "N1", "C2", "C2D", "A2"],
            "location": "Council Bluffs, Iowa, North America",
            "region": "us-central1",
            "zone": "f",
        },
        {
            "machine_types": ["E2", "N2", "N2D", "C3", "T2D", "N1", "M1", "C2", "C2D", "A2", "G2"],
            "location": "Moncks Corner, South Carolina, North America",
            "region": "us-east1",
            "zone": "b",
        },
        {
            "machine_types": ["E2", "N2", "N2D", "C3", "T2D", "N1", "M1", "C2", "C2D"],
            "location": "Moncks Corner, South Carolina, North America",
            "region": "us-east1",
            "zone": "c",
        },        {
            "machine_types": ["E2", "N2", "N2D", "C3", "T2D", "N1", "M1", "C2", "C2D", "G2"],
            "location": "Moncks Corner, South Carolina, North America",
                "region": "us-east1",
            "zone": "d",
        },
        {
            "machine_types": ["E2", "N2", "N2D", "C3", "T2D", "N1", "M1", "M2", "C2", "C2D", "G2"],
            "location": "Ashburn, Virginia, North America",
            "region": "us-east4",
            "zone": "a",
        },
        {
            "machine_types": ["E2", "N2", "N2D", "C3", "T2D", "N1", "M1", "M2", "M3", "C2", "C2D", "G2"],
            "location": "Ashburn, Virginia, North America",
            "region": "us-east4",
            "zone": "b",
        },
        {
            "machine_types": ["E2", "N2", "N2D", "C3", "T2D", "N1", "M1", "C2", "C2D", "A2"],
            "location": "Ashburn, Virginia, North America",
            "region": "us-east4",
            "zone": "c",
        },
    ]

    logger.info(f"writing entities to datastore")
    for properties in property_list:
        # generate random unique uuid id/name for every record
        entity_id = str(uuid.uuid4())
        ds.write_entity(kind, entity_id, properties)


if __name__ == '__main__':
    main()
