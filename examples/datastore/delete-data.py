import google.auth
import json
import os
import uuid

from robles_io_py_gcp import datastore
from robles_io_logger import get_logger

project = os.environ['GOOGLE_PROJECT_ID']


def main():
    logger = get_logger()

    credentials, _ = google.auth.default()
    ds = datastore.Datastore(project, credentials)

    kind = "GCPRegions"
    ds_filter = {
        "and": [
            ("region", "=", "us-central1"),
            ("zone", "=", "b"),
        ],
    }

    entities = ds.get_entities_by_filter(kind, ds_filter)
    for entity in entities:
        logger.info(f"deleting entity {entity.key.name} from datastore")
        ds.delete_entity(kind, entity.key.name)


if __name__ == '__main__':
    main()
