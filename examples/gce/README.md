# Google Compute Engine Examples

To run this example code:

Install the necessary packages
```commandline
pip install robles-io-py-gcp --extra-index-url https://pypi.robles.io/simple
```

Set the necessary environment variables
```commandline
export GOOGLE_APPLICATION_CREDENTIALS='<path to google cloud private key json file>'
export GOOGLE_PROJECT_ID='<target google cloud project id>'
```

Import the needed packages
```
import google.auth
from robles_io_py_gcp import gce
import json
```

Create instance of GCE class
```
credentials, _ = google.auth.default()
gce = gce.GCE(credentials)
```

Set the project, region, and zone
```
project = "robles-io"
region = "us-west1"
zone = "us-west1-a"
```

Create disk
```
disk_body = {
    "name": "robles-io-test",
    "sizeGb": "20"
}

response = gce.create_disk(project, zone, disk_body)
print(json.dumps(response, indent=4, sort_keys=True))
```

Create image
```
image_body = {
    "name": "robles-io-test",
    "family": "robles-io",
    "sourceImage": "projects/ubuntu-os-cloud/global/images/ubuntu-2204-jammy-v20220506"
}

response = gce.create_image(project, image_body)
print(json.dumps(response, indent=4, sort_keys=True))
```

Create instance
```
instance_body = {
    "name": "robles-io-test",
    "machineType": "zones/us-west1-a/machineTypes/n1-standard-1",
    "disks": [
        {
            "autoDelete": "true",
            "boot": "true",
            "initializeParams": {
                "diskSizeGb": "10",
                "sourceImage": "projects/ubuntu-os-cloud/global/images/family/ubuntu-2204-lts"
            }
        }
    ],
    "networkInterfaces": [
        {
            "network": "projects/robles-io/global/networks/robles-io",
            "subnetwork": "regions/us-west1/subnetworks/robles-io-us-west1"
        }
    ],
}

response = gce.create_instance(project, zone, instance_body)
print(json.dumps(response, indent=4, sort_keys=True))
```

List disks
```
disks = gce.list_disks(project, zone, filter="name:robles-io*")
print(json.dumps(disks, indent=4, sort_keys=True))
```

List images
```
images = gce.list_images(project, filter="name:robles-io*")
print(json.dumps(images, indent=4, sort_keys=True))
```

List instances
```
instances = gce.list_instances(project, zone)
print(json.dumps(instances, indent=4, sort_keys=True))
```

List zones
```
zones = gce.list_zones(project, region)
print(json.dumps(zones, indent=4, sort_keys=True))
```

Delete disk
```
response = gce.delete_disk("robles-io-test", project, zone)
print(json.dumps(response, indent=4, sort_keys=True))
```

Delete image
```
response = gce.delete_image("robles-io-test", project)
print(json.dumps(response, indent=4, sort_keys=True))
```

Delete instance
```
response = gce.delete_instance("robles-io-test", project, zone)
print(json.dumps(response, indent=4, sort_keys=True))
```
